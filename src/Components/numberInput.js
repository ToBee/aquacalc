import React from 'react'
import { TextInput } from 'react-native'

const NumberInput = props => {
  const { currentValue, handleChangeText } = props

  return (
    <TextInput
      style={styles.textInput}
      keyboardType={'numeric'}
      blurOnSubmit={true}
      onChangeText={value => handleChangeText(value)}
      value={currentValue}
    />
  )
}

const styles = {
  textInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10,
  },
}

export default NumberInput
