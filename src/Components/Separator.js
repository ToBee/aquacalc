import * as React from 'react'
import { View } from 'react-native'

const Separator = () => <View style={styleToto} />

const styleToto = {
  height: 1,
  backgroundColor: 'blue',
}

export default Separator
