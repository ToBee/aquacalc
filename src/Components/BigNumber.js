import * as React from 'react'
import { Text } from 'react-native'

const Separator = (props) => (
  <Text style={styles.text}>
    {JSON.stringify(props.resultParam)}
  </Text>
)

const styles = {
  container: {
    flex: 1,
  },
  text: {
    fontSize: 18,
  },
}

export default Separator
