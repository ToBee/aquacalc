import React, { Fragment } from 'react'
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
} from 'react-native'

import Basic from './Screens/Basic'

const App = () => {
  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic">
          <Basic />
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  )
}

export default App
