import React from 'react'
import { View } from 'react-native'

import NumberInput from '../Components/numberInput'
import Separator from '../Components/Separator'
import BigNumber from '../Components/BigNumber'

class BasicScreen extends React.PureComponent {
  props;

  state = {
    volumeTotal: '100',
    paramInit: '7',
    volumeChange: '20',
    paramChange: '0',
    resultParam: '6',
  };

  render () {
    return (
      <View style={styles.container}>
        <NumberInput
          style={styles.numberInput}
          handleChangeText={volumeTotal => this.setState({ volumeTotal })}
          currentValue={this.state.volumeTotal}
        />
        <NumberInput
          style={styles.numberInput}
          handleChangeText={paramInit => this.setState({ paramInit })}
          currentValue={this.state.paramInit}
        />
        <NumberInput
          style={styles.numberInput}
          handleChangeText={volumeChange => this.setState({ volumeChange })}
          currentValue={this.state.volumeChange}
        />
        <NumberInput
          style={styles.numberInput}
          handleChangeText={paramChange => this.setState({ paramChange })}
          currentValue={this.state.paramChange}
        />
        <Separator />
        <BigNumber number={this.state.resultParam} />
      </View>
    )
  }
}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    height: '50%',
    padding: 10,
  },
  numberInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10,
  },
}

export default BasicScreen
